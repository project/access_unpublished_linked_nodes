<?php

namespace Drupal\access_unpublished_linked_nodes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;

/**
 * Configure Access Unpublished Linked Nodes settings.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['access_unpublished_linked_nodes.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'access_unpublished_linked_nodes_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('access_unpublished_linked_nodes.settings');

    // Get all content types.
    $node_types = NodeType::loadMultiple();
    $options = [];
    foreach ($node_types as $type) {
      $options[$type->id()] = $type->label();
    }

    $form['node_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select content types to process'),
      '#default_value' => $config->get('node_types') ?: [],
      '#options' => $options,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('access_unpublished_linked_nodes.settings')
      ->set('node_types', array_filter($form_state->getValue('node_types')))
      ->save();
  }

}

