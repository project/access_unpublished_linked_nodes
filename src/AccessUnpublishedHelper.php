<?php

namespace Drupal\access_unpublished_linked_nodes;

use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\Core\File\FileSystemInterface;
use Drupal\node\NodeInterface;

/**
 * Helper class to access and manipulate unpublished content.
 */
class AccessUnpublishedHelper {

  /**
   * Validates the auHash parameter.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node entity.
   * @param string $auHash
   *   The auHash parameter value from the URL.
   *
   * @return bool
   *   TRUE if the auHash is valid, FALSE otherwise.
   */
  public static function validateAuHash(Node $node, $auHash) {
    /** @var \Drupal\access_unpublished\AccessTokenManager $manager */
    $manager = \Drupal::service('access_unpublished.access_token_manager');

    // Retrieve the active access token for the node.
    $token = $manager->getActiveAccessToken($node);
    if ($token && $token->get('value')->value === $auHash) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Modifies the links in the content.
   *
   * @param string $content
   *   The node content.
   * @param string $hashKey
   *   The hashKey for the current node.
   *
   * @return string
   *   The modified content with updated links.
   */
  public static function modifyLinks($content, $hashKey) {
    // Use regular expressions to extract script tags.
    preg_match_all('/<script\b[^>]*>(.*?)<\/script>/is', $content, $matches);
    $scripts = $matches[0];

    // Remove script tags from the content.
    $contentWithoutScripts = preg_replace('/<script\b[^>]*>.*?<\/script>/is', '', $content);

    // Process embedded blocks and render latest revision blocks if available.
    $contentWithoutScripts = self::processEmbeddedBlocks($contentWithoutScripts);

    // Use DOMDocument to parse the remaining content and modify links.
    $dom = new \DOMDocument();
    @$dom->loadHTML(mb_convert_encoding($contentWithoutScripts, 'HTML-ENTITIES', 'UTF-8'));

    $links = $dom->getElementsByTagName('a');
    if (!empty($hashKey)) {
      // Only generate hashKeyed links when needed.
      foreach ($links as $link) {
        $data_entity_uuid = $link->getAttribute('data-entity-uuid');
        if (!empty($data_entity_uuid)) {
          // Load the node by UUID.
          $linked_node = \Drupal::entityTypeManager()
            ->getStorage('node')
            ->loadByProperties(['uuid' => $data_entity_uuid]);
          $linked_node = reset($linked_node);
          if ($linked_node && !$linked_node->isPublished()) {
            // Generate hashKey for the linked node.
            $linked_hashKey = self::getHashTag($linked_node);
            $link->setAttribute('href', $linked_hashKey);
          }
        }
      }
    }

    // Save the modified content.
    $modified_content = $dom->saveHTML();

    // Reinsert the script tags into the modified content.
    foreach ($scripts as $script) {
      $modified_content .= $script;
    }

    return $modified_content;
  }

  /**
   * Gets or generates a hashKey for a node.
   *
   * @param \Drupal\node\Entity\Node $entity
   *   The node entity.
   *
   * @return string
   *   The hashKey.
   */
  public static function getHashTag(Node $entity) {
    $account = User::load(\Drupal::currentUser()->id());
    $hasPermission = FALSE;
    if ($account->hasPermission('access_unpublished node ' . strtolower($entity->getType()))) {
      $hasPermission = TRUE;
    }

    if ($entity instanceof Node && $hasPermission) {
      /** @var \Drupal\access_unpublished\AccessTokenManager $manager */
      $manager = \Drupal::service('access_unpublished.access_token_manager');

      $tokens = $manager->getAccessTokensByEntity($entity, 'active');
      $keys = array_map(function ($token) {
        return $token->get('value')->value;
      }, $tokens);

      if (empty($keys) || count($keys) < 1) {
        $twoHundredDays = 17280000;
        // Create tokens for the entity.
        $token = AccessToken::create([
          'entity_type' => $entity->getEntityType()->id(),
          'entity_id' => $entity->id(),
          'expire' => \Drupal::time()->getRequestTime() + $twoHundredDays,
        ]);
        $token->save();
        $keys = [$token->get('value')->value];
      }

      $countKeys = count($keys);
      $hashToken = '';
      if ($countKeys > 0) {
        $hashToken = [];
        foreach ($keys as $key) {
          $hashToken[] = $key;
        }
        if (isset($hashToken[$countKeys - 1])) {
          $hashToken = $hashToken[$countKeys - 1];
        }
        else {
          $hashToken = '';
        }
      }
      if (isset($hashToken) && strlen($hashToken) > 5) {
        $accessTokenManager = \Drupal::service('access_unpublished.access_token_manager');
        $token = $accessTokenManager->getActiveAccessToken($entity);
        $tokenUrl = '';
        if ($token) {
          $currentLang = \Drupal::languageManager()->getCurrentLanguage();
          $absolute = FALSE;
          $tokenUrl = $accessTokenManager->getAccessTokenUrl($token, $currentLang, $absolute);
        }
        return $tokenUrl;
      }
    }
    // Default below:
    return '';
  }

  /**
   * Checks if the workflow is applied to the bundle.
   *
   * If the node meets the publication conditions.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node entity.
   * @param string $bundle
   *   The bundle name of the node.
   *
   * @return bool
   *   TRUE if the workflow is applied and the conditions are met, FALSE
   *   otherwise.
   */
  public static function isWorkflowApplied(Node $node, $bundle) {
    // Check if the bundle has a workflow applied.
    $workflow_storage = \Drupal::entityTypeManager()->getStorage('workflow');
    $workflows = $workflow_storage->loadMultiple();

    $workflow_applied = FALSE;
    $workflow = NULL;
    foreach ($workflows as $workflow) {
      $configuration = $workflow->getTypePlugin()->getConfiguration();
      if (isset($configuration['bundles']) && is_array($configuration['bundles']) && in_array($bundle, $configuration['bundles'])) {
        $workflow_applied = TRUE;
        break;
      }
    }

    $additional_verifications = FALSE;
    if ($workflow_applied) {
      $states = $workflow->getTypePlugin()->getStates();
      $has_published_state = isset($states['published']);
      $current_state = $node->moderation_state->value;

      // Allow if the node is published, or if the conditions are met.
      if (!$node->isPublished() || ($has_published_state && $current_state !== 'published')) {
        $additional_verifications = TRUE;
      }
    }
    elseif (!$node->isPublished()) {
      $additional_verifications = TRUE;
    }

    return $additional_verifications;
  }

  /**
   * Checks if the current user has allowed roles.
   *
   * @return bool
   *   TRUE if the user has allowed roles, FALSE otherwise.
   */
  public static function roleChecks() {
    $current_user = \Drupal::currentUser();
    $allowed_roles = ['anonymous', 'authenticated', 'viewer'];
    $user_roles = $current_user->getRoles();

    // Return if the user has any roles not in the allowed list.
    foreach ($user_roles as $role) {
      if (!in_array($role, $allowed_roles)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Processes embedded blocks in the content.
   *
   * @param string $content
   *   The content to process.
   *
   * @return string
   *   The content with embedded blocks rendered.
   */
  public static function processEmbeddedBlocks($content) {
    if (!\Drupal::moduleHandler()->moduleExists('embed_block')) {
      return $content;
    }
// <drupal-embed-block class="embed-block" data-block-id="block_content:23d7b334-92f4-416c-ba50-5efd6dcdc8f7">Example block</drupal-embed-block>
    preg_match_all('/<drupal-embed-block\s+.*?data-block-id="([^"]*)">(.*?)<\/drupal-embed-block>/', $content, $matches, PREG_SET_ORDER);
    $processed = [];
    foreach ($matches as $found) {
      $latest_revision_id = 0;
      $block_content_id = 0;
      $block_content = '';
      if (!isset($processed[$found[1]])) {
        try {
          $block_plugin = \Drupal::service('plugin.manager.block')->createInstance($found[1]);
        }
        catch (\Exception $e) {
          \Drupal::logger('access_unpublished_linked_nodes')->error('Error creating block instance for @block: @message', ['@block' => $found[1], '@message' => $e->getMessage()]);
          continue;
        }
        /** @var \Drupal\Core\Block\BlockPluginInterface $block_plugin */
        $block_plugin = \Drupal::service('plugin.manager.block')->createInstance($found[1]);
        // Check if it's a revisionable entity (e.g., custom blocks).
        if ($block_plugin instanceof \Drupal\block_content\Plugin\Block\BlockContentBlock) {
          // Get the block content uuid.
          $block_content_uuid = $block_plugin->getDerivativeId();
          // Load the latest revision of the block content entity, regardless of the published status.
          $block_storage = \Drupal::entityTypeManager()->getStorage('block_content');
          // Load the block entity using the UUID to get the numeric ID.
          $block_entity = \Drupal::entityTypeManager()->getStorage('block_content')->loadByProperties(['uuid' => $block_content_uuid]);
          $block_entity = reset($block_entity); // Get the first (and only) result.
          if ($block_entity) {
            $block_content_id = $block_entity->id();
          }
          // Use a query to fetch the latest revision.
          $query = \Drupal::database()->select('block_content_revision', 'bcr')
            ->fields('bcr', ['revision_id'])
            ->condition('bcr.id', $block_content_id)
            ->orderBy('bcr.revision_created', 'DESC')
            ->range(0, 1);
          $latest_revision_id = $query->execute()->fetchField();

          if ($latest_revision_id) {
            // Load the latest revision of the block content.
            $latest_revision = $block_storage->loadRevision($latest_revision_id);
            if ($latest_revision) {
              // Now create the block plugin instance using the revision.
              $block_view = \Drupal::entityTypeManager()
                ->getViewBuilder('block_content')
                ->view($latest_revision);
              $block_content = \Drupal::service('renderer')->render($block_view);
            }
          }
        }

        // Check if the user has access to the block.
        if (!$latest_revision_id && $block_plugin->access(\Drupal::currentUser())) {
          $build = $block_plugin->build();
          $block_content = \Drupal::service('renderer')->render($build);
        }
        $content = str_replace($found[0], $block_content, $content);
        $processed[$found[1]] = TRUE;
      }
    }

    return $content;
  }

}

