<?php

declare(strict_types=1);

namespace Drupal\access_unpublished_linked_nodes\Plugin\Filter;

use Drupal\access_unpublished_linked_nodes\AccessUnpublishedHelper;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to process links for unpublished nodes.
 *
 * @Filter(
 *   id = "access_unpublished_linked_nodes",
 *   title = @Translation("Access Unpublished Linked Nodes"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   weight = 199
 * )
 */
final class AccessUnpublishedLinkedNodes extends FilterBase {

  /**
   * The route match interface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new AccessUnpublishedLinkedNodes object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    // Load the current node from the route.
    $route_match = \Drupal::routeMatch();
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Method to get the node from the current route.
   *
   * @return \Drupal\node\Entity\Node|null
   *   The node object if found, otherwise NULL.
   */
  public function getNodeFromCurrentRoute() {
    $node = NULL;

    // Get the current route name.
    $route_name = $this->routeMatch->getRouteName();
    // Check if the current route is an entity node canonical route.
    if ($route_name === 'entity.node.canonical' || $route_name === 'entity.node.latest_version') {
      // Get the node parameter from the route.
      $node = $this->routeMatch->getParameter('node');

      // Ensure it is a node entity.
      if ($node instanceof Node) {
        return $node;
      }
    }

    return NULL;
  }

  /**
   * Checks if the current route is the latest revision route for the node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node entity.
   *
   * @return bool
   *   TRUE if the current route is the latest revision route for the node,
   *   FALSE otherwise.
   */
  public function isCurrentRouteLatestRevision(Node $node) {
    // Get the current route.
    $current_route = \Drupal::routeMatch()->getRouteName();

    // Get the route name for the latest version of the node.
    $latest_revision_route = 'entity.node.latest_version';

    // Get the current node ID from the URL parameters.
    $current_node_id = \Drupal::routeMatch()->getParameter('node')->id();

    // Check if the current route matches the latest revision route and the node IDs match.
    if ($current_route == $latest_revision_route && $node->id() == $current_node_id) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $auHash = \Drupal::request()->query->get('auHash');

    $node = $this->getNodeFromCurrentRoute();

    if (is_null($node)) {
      return new FilterProcessResult($text);
    }
    // If the auHash parameter is not present, early exit.
    if (empty($auHash) && is_object($node)) {
      if (!$node->isPublished() || $this->isCurrentRouteLatestRevision($node)) {
        $text = AccessUnpublishedHelper::modifyLinks($text, $auHash);
      }
      return new FilterProcessResult($text);
    }
    $config = \Drupal::config('access_unpublished_linked_nodes.settings');
    $node_types_to_process = $config->get('node_types') ?: ['landing_page', 'page'];
    // Check the content type of the node.
    $content_type = $node->bundle();
    // Check if the content type is in the configured list.
    if (!in_array($content_type, $node_types_to_process)) {
      return new FilterProcessResult($text);
    }
    if (!AccessUnpublishedHelper::validateAuHash($node, $auHash)) {
      return new FilterProcessResult($text);
    }

    if (AccessUnpublishedHelper::roleChecks()) {
      if ($node instanceof Node && AccessUnpublishedHelper::validateAuHash($node, $auHash)) {
        $hashKey = AccessUnpublishedHelper::getHashTag($node);
        $text = AccessUnpublishedHelper::modifyLinks($text, $hashKey);
      }
    }

    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE): string {
    return (string) $this->t('This filter processes links to unpublished nodes and adds access hash keys.');
  }

}

